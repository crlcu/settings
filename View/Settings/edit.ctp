<div class="span12">
    <?php echo $this->Form->create('Setting', array('class' => 'form-horizontal'));?>        
        <?php echo $this->Form->input('id');?>
        <?php echo $this->Form->input('user_id', array('type' => 'hidden', 'value' => $user->id()));?>
        <?php if ( isset($redirect) ):?>
            saved
        <?php endif?>

        <div class="control-group">
            <label class="control-label"><?php echo __('Description')?></label>
            <?php echo $this->Form->input('description', array('escape' => false, 'div' => array('class' => 'controls'), 'label' => false, 'class' => 'span3', 'placeholder' => __('Description')));?>
        </div>
        <?php if ( $this->data['Setting']['type'] == 'boolean' ): ?>
        <div class="control-group">
            <label class="control-label"><?php echo __('Value')?></label>
            <?php echo $this->Form->input('value', array('escape' => false, 'div' => array('class' => 'controls'), 'label' => false, 'options' => array('1' => __('Yes'), '0' => __('No')), 'class' => 'span3', 'placeholder' => __('Value')));?>
        </div>
        <?php else: ?>
        <div class="control-group">
            <label class="control-label"><?php echo __('Value')?></label>
            <?php echo $this->Form->input('value', array('escape' => false, 'div' => array('class' => 'controls'), 'label' => false, 'class' => 'span3', 'placeholder' => __('Value')));?>
        </div>
        <?php endif ?>
    <?php echo $this->Form->end();?>
</div>