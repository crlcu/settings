<?php if ( $this->Paginator->numbers() ) :?>
    <?php
        if ( !isset( $pjax ) ) {
            $pjax = '#page #content';
        }
    ?>
    <div class="pagination pagination-small pagination-right">
        <ul>
            <?php
                // Shows the prev
                echo $this->Paginator->prev('<i class="icon-double-angle-left"></i>', array('escape' => false, 'tag' => 'li', 'class' => 'prev', 'title' => __('Prev'), 'data-pjax' => $pjax), null, array('class' => 'hide'));
                 
                // Shows the page numbers
                echo $this->Paginator->numbers(array('modulus' => 5, 'tag' => 'li', 'currentClass' => 'active', 'separator' => false, 'data-pjax' => $pjax));
                
                //Shows the next
                echo $this->Paginator->next('<i class="icon-double-angle-right"></i>', array('escape' => false, 'tag' => 'li', 'class' => 'next', 'title' => __('Next'), 'data-pjax' => $pjax), null, array('class' => 'hide'));
            ?>
        </ul>
    </div>
<?php endif ?>
